"""
CloudFlareClient description
"""

from __future__ import annotations

import ipaddress
from os import environ

from cloudflare import Cloudflare

from .client import DNSClient


class CloudFlareClient(DNSClient):
    """CloudFlare client class for administering CloudFlare DNS records"""

    def validate_payload(self: CloudFlareClient, _payload: dict) -> None:
        """Python method for validating payload contents"""
        expected_keys = ["domain", "os_envvars", "records"]
        self.logger.debug(f"expected_keys: {expected_keys}")
        for expected_key in expected_keys:
            try:
                assert expected_key in _payload
            except Exception:
                self.logger.error(f"expected_key {expected_key} not present in payload.")

    def load_config(self: CloudFlareClient, _payload: dict) -> None:
        """Python method for loading payload to CloudFlare configurations"""
        envvars = {}
        try:
            for envvar in _payload["os_envvars"]:
                envvars[envvar] = environ[envvar]

            self.settings["os_envvars"] = envvars
            self.logger.debug(f"loaded_vars: {envvars.keys()}")
        except Exception as e:
            self.logger.error(f"Failed to load environment variables. Error: {e}")

        try:
            self.settings["domain"] = _payload["domain"]
            self.records = {record["record"]: record for record in _payload["records"]}
            # self.records = _payload["records"]
        except Exception as e:
            self.logger.error(f"Failed to load cloudflare settings. Missing key error: {e}")

        try:
            self.client = Cloudflare(
                api_email=self.settings["os_envvars"]["CLOUDFLARE_EMAIL"],
                api_key=self.settings["os_envvars"]["CLOUDFLARE_API_KEY"],
            )
        except Exception as e:
            self.logger.error(f"Failed to initialise CloudFlare client. Error: {e}")

    def validate_config(self: CloudFlareClient) -> None:
        """Python method for validating CloudFlare configurations"""
        try:
            self.logger.debug(f"zones: {self.client.zones}")
        except Exception as e:
            self.logger.error(f"Config validation test failed: {e}")

    def add_record(self: CloudFlareClient, _record_name: str, _record_details: dict):
        """add_record for testing"""
        proxied = _record_details.get("proxy", False)
        try:
            self.client.dns.records.create(
                zone_id=self.zone_id,
                name=_record_name,
                content=self.public_ipv4,
                type="A",
                proxied=proxied,
            )
        except Exception as e:
            self.logger.error(f"Failed to add record {_record_name}: {e}")

    def get_record(self: CloudFlareClient, _record_name: str) -> dict:
        """get_record for testing"""
        domain = self.settings["domain"]
        record = None
        try:
            fqdn = _record_name if _record_name == domain else f"{_record_name}.{domain}"
            self.logger.info(f"Search {_record_name} in domain {domain}")
            dns_record = self.dns_records[fqdn]
            self.logger.info(f"Record found {_record_name} in domain {domain}: {dns_record}")
            record = self.settings["domain"] if _record_name == domain else self.records[_record_name]
        except Exception as e:
            self.logger.error(f"Failed to get record {_record_name}: {e}")
        return record

    def update_record(self: CloudFlareClient, _record_name: str, _record_details: dict):
        """update_record for testing"""
        proxied = _record_details.get("proxy", False)
        domain = self.settings["domain"]
        try:
            fqdn = _record_name if _record_name == domain else f"{_record_name}.{domain}"
            record = self.dns_records[fqdn]
            self.logger.info(f"Record to update: {_record_name}")
            self.logger.info(f"zone_id: {self.zone_id}")
            self.logger.info(f"name: {record.name}")
            self.logger.info(f"dns_record_id: {record.id}")
            self.logger.info(f"record content: {self.public_ipv4}")
            self.logger.info(f"proxied: {proxied}")
            self.client.dns.records.update(
                zone_id=self.zone_id,
                name=record.name,
                dns_record_id=record.id,
                content=self.public_ipv4,
                type="A",
                proxied=proxied,
            )
        except Exception as e:
            self.logger.error(f"Failed to update record {_record_name}: {e}")
            self.logger.error(f"zone_id: {self.zone_id}")
            self.logger.error(f"name: {record.name}")
            self.logger.error(f"dns_record_id: {record.id}")
            self.logger.error(f"record content: {self.public_ipv4}")
            self.logger.error(f"proxied: {proxied}")

    def delete_record(self: CloudFlareClient, _record_name: str, _record_details: dict):
        """delete_record for testing"""
        dns_record = self.dns_records[_record_name]

        try:
            self.client.dns.records.delete(zone_id=self.zone_id, dns_record_id=dns_record.id)
        except Exception as e:
            self.logger.error(f"Failed to delete record {_record_name}: {e}")

    def update_ip(self: CloudFlareClient) -> None:
        """update the ip's for each Cloudflare DNS record"""
        records = self.dns_records
        self.logger.info(f"records size: {len(records)}")

        public_ip = self.public_ipv4
        domain_ip = self.dns_records[self.settings["domain"]].content
        self.logger.info(f"public_ip: {public_ip}")
        self.logger.info(f"domain_ip: {domain_ip}")

        if ipaddress.IPv4Address(public_ip) != ipaddress.IPv4Address(domain_ip):
            self.logger.info(f"Updating ip of domain to: {public_ip}")
            self.update_record(_record_name=self.settings["domain"], _record_details={"proxy": True})
            self.logger.info(f"public_ip: {self.public_ipv4}")
            self.logger.info(f"domain_ip: {self.get_record(_record_name=self.settings["domain"])}")

    def synchronise(self: CloudFlareClient) -> None:
        """
        Synchronise DNS records vs configurations.
        """
        self.update_ip()
        self.update_records()
        # T O D O (jesmigel): implement the following
        # - compare config
        # - add when missing
        # - delete when stale

    @property
    def zone_id(self: CloudFlareClient) -> str:
        """Returns of zone id of managed domain registered in CloudFlare"""
        zones = {zone.name: zone.id for zone in self.client.zones.list()}
        return zones[self.settings["domain"]]

    @property
    def dns_records(self: CloudFlareClient) -> dict:
        """Returns of DNS records of zone id's registered in CloudFlare"""
        records = {record.name: record for record in self.client.dns.records.list(zone_id=self.zone_id)}
        return records

    @property
    def client(self):
        """Getter for the Cloudflare client."""
        if self._client is None:
            self.logger.error("Cloudflare client is not initialized.")
        return self._client

    @client.setter
    def client(self, value):
        """Setter for the Cloudflare client."""
        if not isinstance(value, Cloudflare):
            raise ValueError("Invalid client type. Expected a Cloudflare instance.")
        self._client = value
