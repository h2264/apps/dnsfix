"""
DNSClient description TBA
"""

from __future__ import annotations

import concurrent.futures
import logging
from abc import ABC, abstractmethod

from utils import NSUtils


class DNSClient(ABC):
    """Abstract class for DNS service provider clients."""

    def __init__(self: DNSClient, _payload=dict, logger: logging.Logger | None = None) -> None:
        """
        Initialize the DNSClient.

        Args:
            _payload (dict): Configuration payload for DNSClient.
            logger (logging.Logger, optional): Logger instance for logging. Defaults to None.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.nsutils = NSUtils()
        self._client = None  # Private attribute to hold the DNS client instance
        self.settings = {}
        self.records = {}
        self.validate_payload(_payload)
        self.load_config(_payload)
        self.validate_config()
        self.logger.debug(f"payload_yaml={_payload} records={self.records}")

    @abstractmethod
    def validate_payload(self: DNSClient, _payload_yaml: dict) -> None:
        """
        Validate the configuration payload.

        Args:
            _payload_yaml (dict): Configuration payload.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    @abstractmethod
    def load_config(self: DNSClient, _payload_yaml: dict) -> None:
        """
        Load the configuration payload into the client.

        Args:
            _payload_yaml (dict): Configuration payload.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    @abstractmethod
    def validate_config(self: DNSClient) -> None:
        """
        Validate the loaded configuration.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    @abstractmethod
    def add_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
        """
        Add a DNS record.

        Args:
            _record_name (str): Name of the DNS record.
            _record_details (dict): Details of the DNS record.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    def add_records(self: DNSClient) -> None:
        """Add multiple DNS records concurrently."""
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(self.add_record, _record_name=record_name, _record_details=record_details)
                for record_name, record_details in self.records.items()
            ]
            for future in concurrent.futures.as_completed(futures):
                try:
                    future.result()
                except Exception as exc:
                    self.logger.error(f"Exception occurred during add_records: {exc}")

    @abstractmethod
    def get_record(self: DNSClient, _record_name: str) -> dict:
        """
        Retrieve a DNS record.

        Args:
            _record_name (str): Name of the DNS record.

        Returns:
            dict: DNS record details.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    def get_records(self: DNSClient) -> None:
        """Retrieve multiple DNS records concurrently."""
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(self.get_record, _record_name=record_name) for record_name, _ in self.records.items()
            ]
            for future in concurrent.futures.as_completed(futures):
                try:
                    future.result()
                except Exception as exc:
                    self.logger.error(f"Exception occurred during get_records: {exc}")

    @abstractmethod
    def update_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
        """
        Update a DNS record.

        Args:
            _record_name (str): Name of the DNS record.
            _record_details (dict): Updated DNS record details.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    def update_records(self: DNSClient) -> None:
        """Update multiple DNS records concurrently."""
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(self.update_record, _record_name=record_name, _record_details=record_details)
                for record_name, record_details in self.records.items()
            ]
            for future in concurrent.futures.as_completed(futures):
                try:
                    future.result()
                except Exception as exc:
                    self.logger.error(f"Exception occurred during update_records: {exc}")

    @abstractmethod
    def delete_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
        """
        Delete a DNS record.

        Args:
            _record_name (str): Name of the DNS record.
            _record_details (dict): Details of the DNS record to delete.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError

    def delete_records(self: DNSClient):
        """Delete multiple DNS records concurrently."""
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(self.delete_record, _record_name=record_name, _record_details=record_details)
                for record_name, record_details in self.records.items()
            ]
            for future in concurrent.futures.as_completed(futures):
                try:
                    future.result()
                except Exception as exc:
                    self.logger.error(f"Exception occurred during delete_records: {exc}")

    @property
    def public_ipv4(self: DNSClient) -> str:
        """
        Retrieve the public IPv4 address.

        Returns:
            str: Public IPv4 address.
        """
        return self.nsutils.get_public_ip()

    @property
    @abstractmethod
    def client(self: DNSClient):
        """
        DNS client instance.

        Raises:
            NotImplementedError: Must be implemented in subclasses.
        """
        raise NotImplementedError
