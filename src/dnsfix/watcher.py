"""
Module initialisation for the Domain watcher of dnsfix

Author: Jesmigel Cantos
"""

from __future__ import annotations

import logging
from threading import Event

from .manager import ClientManager


class Watcher:
    """Runs the primary process that watches for listed domains."""

    def __init__(
        self: Watcher,
        logger: logging.Logger | None = None,
        polling_rate: float = 1.0,
        payload_yaml: str = "",
    ) -> None:
        # self.dhandler = DomainHandler(payload_yaml=payload_yaml)
        self.dhandler = ClientManager(payload_config=payload_yaml)
        self.logger = logger or logging.getLogger(__name__)
        self.polling_rate = polling_rate
        self.watch = False
        self.exit = Event()

    def start(self: Watcher):
        """Starting the watcher."""
        self.logger.info("starting the watcher")
        self.watch = True
        while not self.exit.is_set():
            self.dhandler.handle()
            self.logger.info(f"self.polling_rate: {self.polling_rate}")
            self.exit.wait(self.polling_rate)

    def stop(self: Watcher, signum=None, frame=None):
        """Stop the watcher."""
        self.logger.info(f"Stopping the watcher due to signal {signum}")
        self.logger.debug(f"Frame: {frame}")
        self.watch = False
        self.exit.set()
