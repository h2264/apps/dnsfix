"""
Entrypoint code of dnsfix python application

Author: Jesmigel Cantos
"""

import argparse
import logging
import re
import signal
import sys

from .watcher import Watcher

logger = logging.getLogger(__name__)


def validate_polling_rate(rate):
    """
    Validates the format of a polling rate string.

    This function validates the format of a polling rate string to ensure it follows the pattern of
    a positive integer followed by a unit representing seconds ('s'), minutes ('m'), or hours ('h').
    For example, '1s' represents 1 second, '1m' represents 1 minute, and '1h' represents 1 hour.

    Args:
        rate (str): The polling rate string to validate.

    Raises:
        argparse.ArgumentTypeError: If the polling rate string does not match the expected format.

    Returns:
        str: The validated polling rate string.

    Example:
        >>> validate_polling_rate('1s')
        '1s'
        >>> validate_polling_rate('5m')
        '5m'
        >>> validate_polling_rate('10h')
        '10h'

    """
    if not re.match(r"^\d+[smh]$", rate):
        raise argparse.ArgumentTypeError(
            """
                Invalid polling rate format.
                Use a number followed by "s" (seconds), "m" (minutes), or "h" (hours).
                Example: 1s, 1m, 1h
            """
        )
    return rate


def convert_to_seconds(rate: str) -> float:
    """
    Converts a polling rate string to seconds.

    This function converts a polling rate string to seconds.
    The input string should be in the format of a positive number followed by a unit.
    Accepted units are in ('s'), minutes ('m'), or hours ('h').
    Example:
        '1s' represents 1 second.
        '5m' represents 5 minutes.
        '10h' represents 10 hours.

    Args:
        rate (str): The polling rate string to convert.

    Returns:
        float: The converted time value in seconds.

    Example:
        >>> convert_to_seconds('1s')
        1.0
        >>> convert_to_seconds('5m')
        300.0
        >>> convert_to_seconds('10h')
        36000.0

    """
    converted_seconds = 0.0
    if rate[-1] == "m":
        converted_seconds = float(rate[:-1]) * 60
    elif rate[-1] == "h":
        converted_seconds = float(rate[:-1]) * 3600
    else:
        converted_seconds = float(rate[:-1])
    return converted_seconds


def parse_args():
    """Parse the command line arguments."""

    parser = argparse.ArgumentParser(
        prog="python -m dnsfix",
        description="DNSFIX controller",
    )
    parser.add_argument(
        "--data",
        type=str,
        required=True,
        help="Path to the data (YAML) file containing commands to execute.",
    )
    parser.add_argument(
        "-p",
        "--polling-rate",
        type=validate_polling_rate,
        default="1s",
        help="Polling rate in the format of time representing a polling rate (e.g., 1s, 1m, 1h)",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Increase verbosity level (-v, -vv, -vvv).",
    )

    return parser.parse_known_args()


if __name__ == "__main__":
    args, unknown = parse_args()
    # Setting verbosity levels
    # 10: DEBUG
    # 20: INFO
    # 40: ERROR
    # 50: CRITICAL
    LOGGING_LEVEL = max(10, logging.INFO - 10 * args.verbose)
    LOG_FORMAT = "%(asctime)s : %(levelname)5s : %(filename)s:%(lineno)s %(funcName)s() : %(msg)s"
    logging.basicConfig(format=LOG_FORMAT, level=LOGGING_LEVEL)

    polling_rate = convert_to_seconds(args.polling_rate)
    logger.info(f"polling_rate: {polling_rate}")
    watcher = Watcher(payload_yaml=args.data, logger=logger, polling_rate=polling_rate)
    signal.signal(signal.SIGTERM, watcher.stop)
    signal.signal(signal.SIGINT, watcher.stop)
    signal.signal(signal.SIGHUP, watcher.stop)
    watcher.start()
    # manager = clientManager(payload_config=args.data, logger=logger, polling_rate=polling_rate)
    # signal.signal(signal.SIGTERM, manager.stop)
    # signal.signal(signal.SIGINT, manager.stop)
    # signal.signal(signal.SIGHUP, manager.stop)
    # manager.start()

    sys.exit()
