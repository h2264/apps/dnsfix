"""
DNS Client Manager python module.

This module provides a `ClientManager` class that initializes and manages DNS clients
based on configuration provided in a YAML file. It supports synchronization operations
for the managed clients and provides robust logging for debugging and error handling.
"""

from __future__ import annotations

import logging

import yaml

from dns import CloudFlareClient


class ClientManager:
    """
    A manager class for handling DNS client operations.

    This class initializes and manages DNS client instances based on configurations
    provided in a YAML file. It supports operations such as loading configurations,
    initializing clients, and synchronizing them.
    """

    def __init__(self: ClientManager, payload_config: str = "", logger: logging.Logger | None = None) -> None:
        """
        Initialize the ClientManager instance.

        Args:
            payload_config (str): Path to the YAML configuration file containing client settings.
            logger (logging.Logger, optional): Custom logger instance. If not provided,
                                               a default logger will be used.

        Attributes:
            clients (dict): A dictionary containing initialized client instances.
            logger (logging.Logger): Logger instance for logging messages.
            sync_clients (bool): A flag indicating whether client synchronization is enabled.
            payload_config (yaml.YAMLObject): Parsed configuration data from the YAML file.

        Raises:
            Exception: If the configuration file cannot be read or parsed.
        """
        self.clients = {}
        self.logger = logger or logging.getLogger(__name__)
        self.sync_clients = False
        self.payload_config = self.load_config(payload_config=payload_config)
        self.init_client()

    def load_config(self: ClientManager, payload_config: str) -> yaml.YAMLObject:
        """
        Load and parse the YAML configuration file.

        Args:
            payload_config (str): Path to the YAML file containing client settings.

        Returns:
            yaml.YAMLObject: Parsed YAML configuration data.

        Raises:
            FileNotFoundError: If the specified file does not exist.
            yaml.YAMLError: If the file cannot be parsed as valid YAML.
        """
        with open(payload_config, "r", encoding="utf-8") as file:
            yaml_data = yaml.safe_load(file)

        self.logger.debug(f"Loaded YAML configuration:\n{yaml.dump(yaml_data)}")
        return yaml_data

    def init_client(self: ClientManager) -> None:
        """
        Initialize DNS clients based on the configuration.

        This method creates and initializes client instances using the configuration
        provided in the YAML file and stores them in the `clients` attribute.

        Logs an error if the initialization fails for any client.

        Raises:
            KeyError: If required configuration keys are missing.
            Exception: For any other errors during client initialization.
        """
        try:
            self.clients["cloudflare"] = CloudFlareClient(_payload=self.payload_config["domains"]["cloudflare"])
            self.logger.info("CloudFlare client successfully initialized.")
        except Exception as e:
            self.logger.error(f"Failed to initialize client: {e}")

    def handle(self: ClientManager) -> None:
        """
        Synchronize all initialized clients.

        This method iterates through all clients in the `clients` attribute and calls
        their `synchronise` method. It logs the synchronization status for each client.

        Raises:
            AttributeError: If a client does not implement the `synchronise` method.
        """
        for client_name, client in self.clients.items():
            self.logger.info(f"Synchronizing client: {client_name}")
            try:
                client.synchronise()
            except AttributeError as e:
                self.logger.error(f"Client {client_name} does not support synchronization: {e}")
