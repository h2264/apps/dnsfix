"""
Module initialisation for the Domain manager of dnsfix

Author: Jesmigel Cantos
"""

from .watcher import Watcher

__all__ = [
    "Watcher",
]
