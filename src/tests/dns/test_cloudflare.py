"""
CloudFlareClient Test Suite
"""

from unittest.mock import MagicMock, PropertyMock, patch

import pytest
from cloudflare import Cloudflare

from dns.cloudflare import CloudFlareClient
from utils.nslookup import NSUtils


@pytest.fixture
def mock_ip():
    """Fixture to mock an IP address."""
    return MagicMock


@pytest.fixture
def payload():
    """Fixture to return a sample payload for initializing CloudFlareClient."""
    return {
        "domain": "example.com",
        "os_envvars": ["CLOUDFLARE_EMAIL", "CLOUDFLARE_API_KEY"],
        "records": [
            {"record": "www", "proxy": True},
            {"record": "a", "proxy": True},
            {"record": "b", "proxy": False},
        ],
    }


@pytest.fixture(
    params=[{"record": "www", "proxy": True}, {"record": "a", "proxy": True}, {"record": "b", "proxy": False}]
)
def dns_record(request):
    """Fixture to return different DNS record configurations for testing."""
    return request.param


@pytest.fixture
def mock_logger():
    """Fixture to return a mock logger instance."""
    return MagicMock()


@pytest.fixture
def mock_cloudflareclient(payload, mock_logger, mock_ip):
    """Fixture to initialize and return a CloudFlareClient with the provided payload."""
    with patch.object(NSUtils, "get_public_ip", return_value=mock_ip), patch.dict(
        "os.environ", {"CLOUDFLARE_EMAIL": "user@example.com", "CLOUDFLARE_API_KEY": "apikey123"}
    ):
        yield CloudFlareClient(_payload=payload, logger=mock_logger)


def test_cloudflare_initialisation(payload):
    """Test CloudFlareClient initialization with environment variables and payload."""
    with patch.dict("os.environ", {"CLOUDFLARE_EMAIL": "user@example.com", "CLOUDFLARE_API_KEY": "apikey123"}):
        client = CloudFlareClient(payload)

        # Validate environment variables are loaded
        for os_envvar in payload["os_envvars"]:
            assert os_envvar in client.settings["os_envvars"]

        assert client.settings["domain"] == payload["domain"]

        # Validate DNS records are initialized correctly
        for record in payload["records"]:
            assert record["record"] in client.records
            assert record == client.records[record["record"]]


@pytest.mark.parametrize(
    "current_keys, expected_log_message",
    [
        ({"os_envvars": "value", "records": "value"}, "expected_key domain not present in payload."),
        ({"domain": "value", "records": "value"}, "expected_key os_envvars not present in payload."),
        ({"domain": "value", "os_envvars": "value"}, "expected_key records not present in payload."),
    ],
)
def test_payload_validation_fail(current_keys, expected_log_message):
    """Test the payload validation failure and check error logging."""
    with patch("dns.cloudflare.CloudFlareClient.load_config"):
        # Create CloudFlareClient instance
        client = CloudFlareClient(current_keys)

        # Mock logger
        client.logger = MagicMock()

        # Call validate_payload and check logger for errors
        client.validate_payload(current_keys)
        client.logger.error.assert_called_with(expected_log_message)


@pytest.mark.parametrize(
    "payload",
    [
        (
            {
                "os_envvars": ["CLOUDFLARE_EMAIL", "CLOUDFLARE_API_KEY"],
                "domain": "example.com",
                "records": [],
            }
        ),
        (
            {
                "os_envvars": ["CLOUDFLARE_EMAIL", "CLOUDFLARE_API_KEY"],
                "domain": "example.com",
                "records": [],
            }
        ),
    ],
)
@patch("dns.cloudflare.CloudFlareClient")
@patch.dict("os.environ", {"CLOUDFLARE_API_KEY": "api_key"}, clear=True)  # Only one env var set
def test_load_config_missing_envvars(mock_cloudflare, payload):
    """Test handling of missing environment variables in load_config."""
    client = CloudFlareClient(payload)
    client.logger = MagicMock()  # Mock the logger

    # Mock Cloudflare client methods
    mock_cloudflare.return_value = MagicMock()

    # Run the method and check error logging
    client.load_config(payload)
    client.logger.error.assert_any_call("Failed to load environment variables. Error: 'CLOUDFLARE_EMAIL'")


@pytest.mark.parametrize(
    "current_keys, expected_log_message",
    [
        (
            {"os_envvars": ["CLOUDFLARE_EMAIL", "CLOUDFLARE_API_KEY"], "records": "value"},
            "Failed to load cloudflare settings. Missing key error: 'domain'",
        ),
        (
            {"os_envvars": ["CLOUDFLARE_EMAIL", "CLOUDFLARE_API_KEY"], "domain": "value"},
            "Failed to load cloudflare settings. Missing key error: 'records'",
        ),
    ],
)
def test_load_config_missing_config_keys(current_keys, expected_log_message):
    """Test handling of missing configuration keys in load_config and check logging."""
    with patch.dict("os.environ", {"CLOUDFLARE_EMAIL": "user@example.com", "CLOUDFLARE_API_KEY": "apikey123"}), patch(
        "dns.cloudflare.CloudFlareClient.validate_payload"
    ):
        client = CloudFlareClient(current_keys)
        client.logger = MagicMock()  # Mock logger

        # Call load_config and check for logged error message
        client.load_config(current_keys)
        client.logger.error.assert_called_once_with(expected_log_message)


def test_add_records(mock_cloudflareclient, dns_record, mock_ip):
    """Test adding DNS records to CloudFlare."""
    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id:
        mock_cloudflareclient.add_records()

        # Verify DNS records are added
        assert mock_cloudflare.dns.records.create.call_count == 3
        mock_cloudflare.dns.records.create.assert_any_call(
            zone_id=mock_zone_id,
            name=dns_record["record"],
            content=mock_ip,
            type="A",
            proxied=dns_record["proxy"],
        )


def test_add_records_fail(mock_cloudflareclient, dns_record, mock_ip):
    """Test failure when adding DNS records to CloudFlare."""
    error_message = "Simulated DNS record creation failure"

    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id:

        # Simulate failure in DNS record creation
        mock_cloudflare.dns.records.create.side_effect = Exception(error_message)

        mock_cloudflareclient.add_records()

        assert mock_cloudflare.dns.records.create.call_count == 3
        mock_cloudflare.dns.records.create.assert_any_call(
            zone_id=mock_zone_id,
            name=dns_record["record"],
            content=mock_ip,
            type="A",
            proxied=dns_record["proxy"],
        )

        # Check that error is logged
        mock_cloudflareclient.logger.error.assert_any_call(
            f"Failed to add record {dns_record['record']}: {error_message}"
        )


def test_update_records(mock_cloudflareclient, dns_record, mock_ip):
    """Test updating DNS records in CloudFlare."""
    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id, patch("dns.cloudflare.CloudFlareClient.dns_records") as mock_dns_records:
        mock_cloudflareclient.update_records()

        # Verify that DNS records are updated correctly
        assert mock_cloudflare.dns.records.update.call_count == 3
        mock_dns_record = mock_dns_records[dns_record]
        mock_cloudflare.dns.records.update.assert_any_call(
            zone_id=mock_zone_id,
            name=mock_dns_record.name,
            dns_record_id=mock_dns_record.id,
            content=mock_ip,
            type="A",
            proxied=dns_record["proxy"],
        )


def test_update_records_fail(mock_cloudflareclient, dns_record, mock_ip):
    """Test failure when updating DNS records in CloudFlare."""
    error_message = "Simulated DNS record update failure"

    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id, patch("dns.cloudflare.CloudFlareClient.dns_records") as mock_dns_records:

        # Configure the mock to raise an exception when dns.records.update is called
        mock_cloudflare.dns.records.update.side_effect = Exception(error_message)

        mock_cloudflareclient.update_records()

        # Verify the exact number of calls
        assert mock_cloudflare.dns.records.update.call_count == 3

        # Verify the arguments for current call
        fqdn = f"{dns_record}.{mock_cloudflare.settings['domain']}"
        mock_dns_record = mock_dns_records[fqdn]
        # mock_cloudflare.dns.records.update.assert_called_once()
        mock_cloudflare.dns.records.update.assert_any_call(
            zone_id=mock_zone_id,
            name=mock_dns_record.name,
            dns_record_id=mock_dns_record.id,
            content=mock_ip,
            type="A",
            proxied=dns_record["proxy"],
        )

        # Verify that error is logged
        mock_cloudflareclient.logger.error.assert_any_call(
            f"Failed to update record {dns_record['record']}: {error_message}"
        )
        mock_cloudflareclient.logger.error.assert_any_call(f"zone_id: {mock_zone_id}")
        mock_cloudflareclient.logger.error.assert_any_call(f"name: {mock_dns_record.name}")
        mock_cloudflareclient.logger.error.assert_any_call(f"dns_record_id: {mock_dns_record.id}")
        mock_cloudflareclient.logger.error.assert_any_call(f"record content: {mock_ip}")
        mock_cloudflareclient.logger.error.assert_any_call(f"proxied: {dns_record["proxy"]}")


def test_delete_records(mock_cloudflareclient, dns_record):
    """Test successful deletion of DNS records."""
    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id, patch("dns.cloudflare.CloudFlareClient.dns_records") as mock_dns_records:
        mock_cloudflareclient.delete_records()

        mock_dns_record_id = mock_dns_records[dns_record].id
        # Verify the exact number of calls
        assert mock_cloudflare.dns.records.delete.call_count == 3

        # Verify the arguments for current call
        mock_cloudflare.dns.records.delete.assert_any_call(zone_id=mock_zone_id, dns_record_id=mock_dns_record_id)


def test_delete_records_fail(mock_cloudflareclient, dns_record):
    """Test DNS record deletion failure and logging."""
    error_message = "Simulated DNS record delete failure"

    with patch("dns.cloudflare.CloudFlareClient.client") as mock_cloudflare, patch(
        "dns.cloudflare.CloudFlareClient.zone_id"
    ) as mock_zone_id, patch("dns.cloudflare.CloudFlareClient.dns_records") as mock_dns_records:

        # Configure the mock to raise an exception when dns.records.delete is called
        mock_cloudflare.dns.records.delete.side_effect = Exception(error_message)

        mock_cloudflareclient.delete_records()

        mock_dns_record_id = mock_dns_records[dns_record].id

        # Verify the exact number of calls
        assert mock_cloudflare.dns.records.delete.call_count == 3

        # Verify the arguments for current call
        mock_cloudflare.dns.records.delete.assert_any_call(zone_id=mock_zone_id, dns_record_id=mock_dns_record_id)

        # Verify that error is logged
        mock_cloudflareclient.logger.error.assert_any_call(
            f"Failed to delete record {dns_record['record']}: {error_message}"
        )


def test_get_record(mock_cloudflareclient, mock_logger, dns_record):
    """Test successful retrieval of a DNS record."""
    with patch("dns.cloudflare.CloudFlareClient.dns_records") as mock_dns_records:
        # Mock dns_records and client behavior
        domain = mock_cloudflareclient.settings["domain"]
        record_name = dns_record["record"]
        fqdn = f"{record_name}.{domain}"
        mock_dns_records[fqdn] = {"type": "A", "value": "1.2.3.4"}
        mock_cloudflareclient.settings = {"domain": domain}
        mock_cloudflareclient.records = {record_name: {"type": "A", "value": "1.2.3.4"}}

        # Call get_record
        record = mock_cloudflareclient.get_record(_record_name=record_name)

        # Assertions
        assert record == mock_cloudflareclient.records[record_name]
        mock_logger.info.assert_any_call(f"Search {record_name} in domain {domain}")
        mock_logger.info.assert_any_call(f"Record found {record_name} in domain {domain}: {mock_dns_records[fqdn]}")


def test_get_record_fail(mock_cloudflareclient, mock_logger):
    """Test DNS record retrieval failure and logging."""
    domain = "example.com"
    record_name = "test"

    # Setup mock_cloudflareclient settings
    mock_cloudflareclient.settings = {"domain": domain}
    mock_cloudflareclient.records = {}

    # Patch the dns_records property to simulate behavior
    with patch("dns.cloudflare.CloudFlareClient.dns_records", new_callable=PropertyMock):
        # Simulate missing record by raising a KeyError
        # mock_dns_records.__getitem__.side_effect = KeyError(fqdn)

        # Call get_record (expected to fail due to missing record)
        record = mock_cloudflareclient.get_record(record_name)

        # Assertions
        assert record is None  # Expecting None due to failure

        # Verify logging of the error
        mock_logger.error.assert_called_with(f"Failed to get record {record_name}: '{record_name}'")


def test_update_ip(mock_cloudflareclient):
    """Test updating IP address for DNS records."""
    with patch("dns.cloudflare.CloudFlareClient.public_ipv4", new_callable=PropertyMock) as mock_public_ipv4, patch(
        "dns.cloudflare.CloudFlareClient.dns_records", new_callable=PropertyMock
    ) as mock_dns_records, patch("dns.cloudflare.CloudFlareClient.update_record") as mock_update_record:

        # Mock values for public_ipv4 and dns_records
        mock_public_ipv4.return_value = "192.168.1.1"
        mock_dns_records.return_value = {"example.com": type("MockRecord", (object,), {"content": "192.168.1.2"})()}

        # Mock settings attribute in the CloudFlareClient instance
        mock_cloudflareclient.settings = {"domain": "example.com"}

        # Call the method under test
        mock_cloudflareclient.update_ip()

        # Assertions
        assert mock_dns_records.call_count == 3
        assert mock_public_ipv4.call_count == 2
        # mock_update_records.assert_called_once()
        mock_update_record.assert_called_once_with(_record_name="example.com", _record_details={"proxy": True})

        # Verify logger calls
        mock_cloudflareclient.logger.info.assert_any_call("records size: 1")
        mock_cloudflareclient.logger.info.assert_any_call("public_ip: 192.168.1.1")
        mock_cloudflareclient.logger.info.assert_any_call("domain_ip: 192.168.1.2")
        mock_cloudflareclient.logger.info.assert_any_call("Updating ip of domain to: 192.168.1.1")


def test_synchronise(mock_cloudflareclient):
    """Test synchronization of CloudFlareClient."""
    with patch("dns.cloudflare.CloudFlareClient.update_ip", new_callable=PropertyMock) as mock_update_ip, patch(
        "dns.cloudflare.CloudFlareClient.update_records"
    ) as mock_update_records:
        mock_cloudflareclient.synchronise()
        mock_update_ip.assert_called_once()
        mock_update_records.assert_called_once()


def test_dns_records(mock_cloudflareclient):
    """Test fetching and validating DNS records."""
    with patch("dns.cloudflare.CloudFlareClient.client") as mock_client, patch(
        "dns.cloudflare.CloudFlareClient.zone_id", new_callable=PropertyMock
    ) as mock_zone_id:

        # Mock the zone_id value
        mock_zone_id.return_value = "example-zone-id"

        # Mock the client.dns.records.list response
        mock_client.dns.records.list.return_value = [
            MagicMock(name="www.example.com", content="192.168.1.1"),
            MagicMock(name="api.example.com", content="192.168.1.2"),
        ]

        # Explicitly set the name attributes of the mocked records
        mock_client.dns.records.list.return_value[0].name = "www.example.com"
        mock_client.dns.records.list.return_value[1].name = "api.example.com"

        # Access the dns_records property
        records = mock_cloudflareclient.dns_records

        # Assertions
        assert len(records) == 2
        assert "www.example.com" in records
        assert records["www.example.com"].content == "192.168.1.1"
        assert "api.example.com" in records
        assert records["api.example.com"].content == "192.168.1.2"


def test_client_setter():
    """Test setter for CloudFlareClient's client attribute."""
    cloudflare_client = CloudFlareClient()

    # Test valid client type
    valid_client = MagicMock(spec=Cloudflare)
    cloudflare_client.client = valid_client

    # Test invalid client type
    invalid_client = MagicMock()  # Does not match Cloudflare type
    with pytest.raises(ValueError, match="Invalid client type. Expected a Cloudflare instance."):
        cloudflare_client.client = invalid_client
