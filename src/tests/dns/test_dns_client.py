"""
DNSClient Test Suite
"""

from __future__ import annotations

import logging
from typing import Any, Dict
from unittest import mock

import pytest

from dns.client import DNSClient
from utils import NSUtils


class MockDNSClient(DNSClient):
    """Concrete mock class for testing"""

    def validate_payload(self: MockDNSClient, _payload: dict) -> None:
        """Mock validate_payload method for testing purposes"""
        self.records["operation"] = f"payload validated: {_payload}"

    def load_config(self: MockDNSClient, _payload: dict) -> None:
        """Mock load_config method for testing purposes"""
        self.records["operation"] = f"config loaded: {_payload}"

    def validate_config(self: MockDNSClient) -> None:
        """Mock validate_config method for testing purposes"""
        self.records["operation"] = "config validated"

    def add_record(self: MockDNSClient, _record_name: str, _record_details: dict) -> None:
        """Mock add_record method for testing purposes"""
        self.records[_record_name] = _record_details

    def get_record(self: MockDNSClient, _record_name: str) -> Dict[Any]:
        """Mock get_record method for testing purposes"""
        record = []
        if _record_name in self.records:
            record = self.records[_record_name]
        return record

    def update_record(self: MockDNSClient, _record_name: str, _record_details: dict) -> None:
        """Mock update_record method for testing purposes"""
        self.records[_record_name] = _record_details

    def delete_record(self: MockDNSClient, _record_name: str, _record_details: dict) -> None:
        """Mock delete_record method for testing purposes"""
        if _record_name in self.records:
            del self.records[_record_name]

    @property
    def client(self: MockDNSClient) -> Any:
        """Mock DNS client property for testing purposes"""
        return mock.MagicMock()


@pytest.fixture
def logger():
    """Fixture for creating a mock logger instance."""
    return mock.Mock()


@pytest.fixture
def mock_dnsclient(logger):
    """Fixture for creating a mock MockDNSClient instance."""
    return MockDNSClient(logger=logger)


@pytest.fixture(
    params=[
        {
            "record1": {"field1": "value1"},
            "record2": {"field2": "value2"},
            "record3": {"field3": "value3"},
        }
    ]
)
def mock_records(request):
    """Fixture to provide mock records for testing."""
    return request.param


def test_not_implemented_errors():
    """
    Test that calling the abstract methods (e.g., validate_payload, load_config,
    validate_config, add_record, etc.) of a subclass of DNSClient without overriding them
    raises a NotImplementedError.
    """

    # Create a subclass of DNSClient to test unimplemented methods
    class TestDNSClient(DNSClient):
        """
        A subclass of DNSClient that does not implement the abstract methods
        to test the behavior of unimplemented methods.
        """

        def __init__(self, logger: logging.Logger | None = None):
            """
            Initialize the TestDNSClient subclass, with an optional logger.
            If no logger is provided, a default logger is used.

            Args:
                logger (logging.Logger, optional): The logger instance to use. Defaults to None.
            """
            self.logger = logger or logging.getLogger(__name__)

        def load_config(self: DNSClient, _payload: dict) -> None:
            """
            Inherit and call the base class's load_config method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _payload (dict): The payload used for configuration loading.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().load_config(_payload)

        def add_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
            """
            Inherit and call the base class's add_record method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _record_name (str): The name of the record to add.
                _record_details (dict): The details of the record to add.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().add_record(_record_name, _record_details)

        def delete_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
            """
            Inherit and call the base class's delete_record method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _record_name (str): The name of the record to delete.
                _record_details (dict): The details of the record to delete.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().delete_record(_record_name, _record_details)

        def get_record(self: DNSClient, _record_name: str) -> dict:
            """
            Inherit and call the base class's get_record method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _record_name (str): The name of the record to retrieve.

            Returns:
                dict: The record details.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().get_record(_record_name)

        def update_record(self: DNSClient, _record_name: str, _record_details: dict) -> None:
            """
            Inherit and call the base class's update_record method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _record_name (str): The name of the record to update.
                _record_details (dict): The updated record details.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().update_record(_record_name, _record_details)

        def validate_config(self: DNSClient) -> None:
            """
            Inherit and call the base class's validate_config method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().validate_config()

        def validate_payload(self: DNSClient, _payload: dict) -> None:
            """
            Inherit and call the base class's validate_payload method to simulate an unimplemented method.
            This method will raise a NotImplementedError.

            Args:
                _payload (dict): The payload to validate.

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().validate_payload(_payload)

        @property
        def client(self: DNSClient) -> Any:
            """
            Inherit and call the base class's client property method to simulate an unimplemented method.
            This property will raise a NotImplementedError.

            Returns:
                Any: The client instance (mocked in this case).

            Raises:
                NotImplementedError: This method should be overridden by subclasses.
            """
            return super().client

    # Test that NotImplementedError is raised for unimplemented methods
    dns_client = TestDNSClient(logger=mock.MagicMock())
    test_payload = {}
    test_record_name = ""
    test_record_details = {}

    with pytest.raises(NotImplementedError):
        dns_client.load_config(_payload=test_payload)

    with pytest.raises(NotImplementedError):
        dns_client.validate_payload(_payload=test_payload)

    with pytest.raises(NotImplementedError):
        dns_client.validate_config()

    with pytest.raises(NotImplementedError):
        dns_client.add_record(_record_name=test_record_name, _record_details=test_record_details)

    with pytest.raises(NotImplementedError):
        dns_client.get_record(_record_name=test_record_name)

    with pytest.raises(NotImplementedError):
        dns_client.update_record(_record_name=test_record_name, _record_details=test_record_details)

    with pytest.raises(NotImplementedError):
        dns_client.delete_record(_record_name=test_record_name, _record_details=test_record_details)

    with pytest.raises(NotImplementedError):
        _ = dns_client.client


@pytest.fixture
def mock_dns_client_fixture():
    """
    Fixture that mocks the validate_payload, load_config, and validate_config methods
    for the MockDNSClient class. The fixture provides a mocked instance of MockDNSClient
    for use in tests.

    Returns:
        tuple: A tuple containing the payload, and the mocked methods for
               validate_payload, load_config, and validate_config.
    """
    # Mock the validate_payload, load_config, and validate_config methods
    with mock.patch.object(MockDNSClient, "validate_payload") as mock_validate_payload, mock.patch.object(
        MockDNSClient, "load_config"
    ) as mock_load_config, mock.patch.object(MockDNSClient, "validate_config") as mock_validate_config:

        # Create an instance of the MockDNSClient with a sample payload
        payload = {"dns_provider": "Cloudflare", "api_key": "dummy_api_key"}
        _ = MockDNSClient(_payload=payload)

        # Return the client instance and the mocks so we can assert against them
        yield payload, mock_validate_payload, mock_load_config, mock_validate_config


def test_dns_client_constructor(mock_dns_client_fixture):
    """Test that validate_payload load_config and is validate_config called during DNSClient initialization."""
    # Unpack the fixture
    payload, mock_validate_payload, mock_load_config, mock_validate_config = mock_dns_client_fixture

    # Assertions to check that methods were called during the constructor
    mock_validate_payload.assert_called_once_with(payload)
    mock_load_config.assert_called_once_with(payload)
    mock_validate_config.assert_called_once()


def test_add_records_call_add_record(mock_dnsclient):
    """Test that add_record is called when adding a record."""
    with mock.patch.object(mock_dnsclient, "add_record", autospec=True) as mock_add_record:
        _record_details = mock.Mock()
        _record_name = "record1"
        mock_dnsclient.add_records()
        mock_add_record.assert_called_once()

    mock_dnsclient.add_record(_record_name, _record_details)
    assert mock_dnsclient.records[_record_name] == _record_details


def test_update_records_call_update_record(mock_dnsclient):
    """Test that update_record is called when updating a record."""
    with mock.patch.object(mock_dnsclient, "update_record", autospec=True) as mock_update_record:
        _record_details = mock.Mock()
        _record_name = "record1"
        mock_dnsclient.update_records()
        mock_update_record.assert_called_once()

    mock_dnsclient.add_record(_record_name, _record_details)
    mock_dnsclient.update_record(_record_name, mock.Mock())
    assert mock_dnsclient.records[_record_name] != _record_details


def test_get_records_call_get_record(mock_dnsclient):
    """Test that get_record is called when fetching a record."""
    with mock.patch.object(mock_dnsclient, "get_record", autospec=True) as mock_get_record:
        _record_name = "record1"
        mock_dnsclient.get_records()
        mock_get_record.assert_called_once()

    _record_details = mock.Mock()
    mock_dnsclient.add_record(_record_name, _record_details)
    assert mock_dnsclient.get_record(_record_name) == _record_details


def test_delete_records_call_delete_record(mock_dnsclient):
    """Test that delete_record is called when deleting a record."""
    with mock.patch.object(mock_dnsclient, "delete_record", autospec=True) as mock_delete_record:
        _record_details = mock.Mock()
        _record_name = "record1"
        mock_dnsclient.delete_records()
        mock_delete_record.assert_called_once()

    _record_details = mock.Mock()
    mock_dnsclient.add_record(_record_name, _record_details)
    mock_dnsclient.delete_record(_record_name, _record_details)
    assert _record_name not in mock_dnsclient.records.keys()


def test_add_records_multithread_success(mock_dnsclient, mock_records):
    """Test that add_records handles success with multiple threads."""
    mock_dnsclient.records = mock_records
    with mock.patch.object(mock_dnsclient, "add_record", autospec=True) as mock_add_record:
        mock_add_record.side_effect = [None, None, None]  # No exception, all succeed

        mock_dnsclient.add_records()

        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_add_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_add_record.call_count == len(mock_dnsclient.records)


def test_add_records_multithread_failure(mock_dnsclient, mock_records):
    """Test that add_records handles failure with multiple threads."""
    mock_dnsclient.records = mock_records
    with mock.patch.object(mock_dnsclient, "add_record", autospec=True) as mock_add_record:
        mock_add_record.side_effect = [
            Exception("Failed to add record1"),
            Exception("Failed to add record2"),
            Exception("Failed to add record3"),
        ]

        mock_dnsclient.add_records()

        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_add_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_add_record.call_count == len(mock_dnsclient.records)


def test_update_records_multithread_success(mock_dnsclient, mock_records):
    """Test that update_records handles success with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the update_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "update_record", autospec=True) as mock_update_record:
        mock_update_record.side_effect = [None, None, None]  # No exception, all succeed

        mock_dnsclient.update_records()

        # Verify update_record was called for each record
        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_update_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_update_record.call_count == len(mock_dnsclient.records)


def test_update_records_multithread_failure(mock_dnsclient, mock_records):
    """Test that update_records handles failure with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the update_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "update_record", autospec=True) as mock_update_record:
        mock_update_record.side_effect = [
            Exception("Failed to update record1"),
            Exception("Failed to update record2"),
            Exception("Failed to update record3"),
        ]

        mock_dnsclient.update_records()

        # Verify update_record was called for each record
        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_update_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_update_record.call_count == len(mock_dnsclient.records)


def test_get_records_multithread_success(mock_dnsclient, mock_records):
    """Test that get_records handles success with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the get_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "get_record", autospec=True) as mock_get_record:
        mock_get_record.side_effect = [None, None, None]  # No exception, all succeed

        mock_dnsclient.get_records()

        # Verify get_record was called for each record
        expected_calls = [
            mock.call("record1"),
            mock.call("record2"),
            mock.call("record3"),
        ]
        mock_get_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_get_record.call_count == len(mock_dnsclient.records)


def test_get_records_multithread_failure(mock_dnsclient, mock_records):
    """Test that get_records handles failure with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the get_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "get_record", autospec=True) as mock_get_record:
        mock_get_record.side_effect = [
            Exception("Failed to get record1"),
            Exception("Failed to get record2"),
            Exception("Failed to get record3"),
        ]

        mock_dnsclient.get_records()

        # Verify get_record was called for each record
        expected_calls = [
            mock.call("record1"),
            mock.call("record2"),
            mock.call("record3"),
        ]
        mock_get_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_get_record.call_count == len(mock_dnsclient.records)


def test_delete_records_multithread_success(mock_dnsclient, mock_records):
    """Test that delete_records handles success with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the delete_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "delete_record", autospec=True) as mock_delete_record:
        mock_delete_record.side_effect = [None, None, None]  # No exception, all succeed

        mock_dnsclient.delete_records()

        # Verify delete_record was called for each record
        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_delete_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_delete_record.call_count == len(mock_dnsclient.records)


def test_delete_records_multithread_failure(mock_dnsclient, mock_records):
    """Test that delete_records handles failure with multiple threads."""
    mock_dnsclient.records = mock_records
    # Mock the delete_record method to simulate complete success
    with mock.patch.object(mock_dnsclient, "delete_record", autospec=True) as mock_delete_record:
        mock_delete_record.side_effect = [
            Exception("Failed to delete record1"),
            Exception("Failed to delete record2"),
            Exception("Failed to delete record3"),
        ]

        mock_dnsclient.delete_records()

        # Verify delete_record was called for each record
        expected_calls = [
            mock.call("record1", {"field1": "value1"}),
            mock.call("record2", {"field2": "value2"}),
            mock.call("record3", {"field3": "value3"}),
        ]
        mock_delete_record.assert_has_calls(expected_calls, any_order=True)
        assert mock_delete_record.call_count == len(mock_dnsclient.records)


def test_get_public_ip(mock_dnsclient):
    """Test that public_ipv4 calls get_public_ip from NSUtils."""
    with mock.patch.object(NSUtils, "get_public_ip") as mock_get_public_ip:
        _ = mock_dnsclient.public_ipv4
        mock_get_public_ip.assert_called_once_with()


def test_mockdnsclient_client_property(mock_dnsclient):
    """Test that client can be called."""
    # Access the client property
    client = mock_dnsclient.client

    # Assertions
    assert isinstance(client, mock.MagicMock), "The client property should return a MagicMock instance."
    assert client is not None, "The client property should not return None."

    # Check that the mock supports arbitrary attribute access
    client.some_method.return_value = "mocked result"
    assert client.some_method() == "mocked result"
