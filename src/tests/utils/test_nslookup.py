"""
Module initialization for name server utilities used by DNSFix.
"""

import socket
from unittest.mock import MagicMock, patch

import pytest
import requests

from utils import NSUtils


@pytest.fixture
def ns_utils():
    """Fixture to initialize NSUtils with a mock logger."""
    logger = MagicMock()
    return NSUtils(logger=logger)


def test_lookup_valid_hostname(ns_utils):
    """
    Test case for a valid hostname lookup using NSUtils.
    Ensures the correct IP address is returned.
    """
    with patch("socket.gethostbyname", return_value="127.0.0.1") as mock_gethostbyname:
        ip = ns_utils.lookup("localhost")
        assert ip == "127.0.0.1"
        mock_gethostbyname.assert_called_once_with("localhost")
        ns_utils.logger.info.assert_any_call("Name: localhost")
        ns_utils.logger.info.assert_any_call("Address: 127.0.0.1")


def test_lookup_invalid_hostname(ns_utils):
    """
    Test case for an invalid hostname lookup using NSUtils.
    Ensures None is returned, and a warning is logged.
    """
    with patch("socket.gethostbyname", side_effect=socket.gaierror("Host not found")):
        ip = ns_utils.lookup("nonexistent.host")
        assert ip is None
        ns_utils.logger.warning.assert_called_once_with("Failed to lookup nonexistent.host: Host not found")


def test_get_public_ip_success(ns_utils):
    """
    Test case for successful public IP retrieval using NSUtils.
    Ensures the correct IP address is returned.
    """
    mock_response = MagicMock()
    mock_response.status_code = 200
    mock_response.text = "203.0.113.5"
    with patch("requests.get", return_value=mock_response):
        ip = ns_utils.get_public_ip()
        assert ip == "203.0.113.5"
        ns_utils.logger.warning.assert_not_called()


def test_get_public_ip_failure(ns_utils):
    """
    Test case for failed public IP retrieval using NSUtils.
    Ensures None is returned, and a warning is logged.
    """
    mock_response = MagicMock()
    mock_response.status_code = 404
    with patch("requests.get", return_value=mock_response):
        ip = ns_utils.get_public_ip()
        assert ip is None
        ns_utils.logger.warning.assert_called_once_with("Failed to retrieve public IP: 404")


def test_get_public_ip_exception(ns_utils):
    """
    Test case for exceptions during public IP retrieval using NSUtils.
    Ensures None is returned, and an error is logged.
    """
    with patch("requests.get", side_effect=requests.RequestException("Request failed")):
        ip = ns_utils.get_public_ip()
        assert ip is None
        ns_utils.logger.error.assert_called_once_with("An error occurred: Request failed")


def test_get_domain_ip_valid(ns_utils):
    """
    Test case for a valid domain IP lookup using NSUtils.
    Ensures the correct IP address is returned.
    """
    with patch("socket.gethostbyname", return_value="93.184.216.34") as mock_gethostbyname:
        ip = ns_utils.get_domain_ip("example.com")
        mock_gethostbyname.assert_called_once_with("example.com")
        assert ip == "93.184.216.34"
        ns_utils.logger.debug.assert_called_once_with("fqdn: example.com ip_address: 93.184.216.34")


def test_get_domain_ip_invalid(ns_utils):
    """
    Test case for an invalid domain IP lookup using NSUtils.
    Ensures None is returned, and an error is logged.
    """
    with patch("socket.gethostbyname", side_effect=socket.gaierror("DNS lookup failed")):
        ip = ns_utils.get_domain_ip("invalid.domain")
        assert ip is None
        ns_utils.logger.error.assert_called_once_with("Exception caught: DNS lookup failed")


def test_get_domain_ip_custom_dns_server(ns_utils):
    """
    Test case for domain IP lookup using a custom DNS server with NSUtils.
    Simulates a custom DNS server response.
    """
    with patch("socket.socket") as mock_socket:
        mock_instance = mock_socket.return_value
        mock_instance.recv.return_value = b"\x00\x00\x00\x00" + b"\xc0\xa8\x00\x01"  # Simulated IP: 192.168.0.1
        ip = ns_utils.get_domain_ip("example.com", dns_server="8.8.8.8")
        assert ip == "192.168.0.1"
        ns_utils.logger.debug.assert_called_once_with("fqdn: example.com ip_address: 192.168.0.1")
