"""
Module initialisation for name server utilities used by DNSFix
"""

from __future__ import annotations

import logging
import socket

import requests


class NSUtils:
    """
    NSUtils class provides methods for network-related utilities.

    This class is responsible for determining the public IP address where the code is executed,
    as well as interrogating a DNS server to check if a domain name has a corresponding IP address.

    Attributes:
        None

    Methods:
        get_public_ip(): Retrieves the public IP address where the code is executed.
        get_domain_ip(domain_name, dns_server=None): Interrogates a DNS server to check if
            a domain name has a corresponding IP address. If no DNS server is specified,
            it uses the default system DNS resolver. Returns the IP address if found,
            otherwise returns None.
    """

    def __init__(self: NSUtils, logger: logging.Logger | None = None):
        """NSUtils constructor."""
        self.logger = logger or logging.getLogger(__name__)

    def lookup(self: NSUtils, hostname: str = "localhost") -> str | None:
        """
        Get public IPv4 of provided fully qualified domain name using a nameserver.
        Args:
            domain_name (str): The name of the domain to look.
            dns_server (str): IPv4 of the nameserver
        Returns:
            ip_address (str): The IPv4 address of the provided fully qualified domain name.
        """
        try:
            ip_address = socket.gethostbyname(hostname)
            self.logger.info(f"Name: {hostname}")
            self.logger.info(f"Address: {ip_address}")
            return str(ip_address)
        except socket.gaierror as e:
            self.logger.warning(f"Failed to lookup {hostname}: {e}")
            return None

    def get_public_ip(self: NSUtils) -> str | None:
        """Get Public IPv4"""
        try:
            response = requests.get("https://api.ipify.org", timeout=5)
            if response.status_code == 200:
                return response.text
            self.logger.warning(f"Failed to retrieve public IP: {response.status_code}")
            return None
        except Exception as e:
            self.logger.error(f"An error occurred: {e}")
            return None

    def get_domain_ip(self: NSUtils, domain_name: str, dns_server: str | None = None) -> str:
        """
        Get public IPv4 of provided fully qualified domain name using a nameserver.
        Args:
            domain_name (str): The name of the domain to look.
            dns_server (str): IPv4 of the nameserver
        Returns:
            ip_address (str): The IPv4 address of the provided fully qualified domain name.
        """
        try:
            if dns_server:
                resolver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                resolver.settimeout(2)  # Set a timeout for the DNS query
                resolver.connect((dns_server, 53))
                resolver.sendall(
                    b"\x00\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00"
                    + bytes("".join(chr(len(x)) + x for x in domain_name.split(".")), "utf-8")
                    + b"\x00\x00\x01\x00\x01"
                )
                response = resolver.recv(1024)
                ip_address = ".".join(map(str, response[-4:]))
            else:
                ip_address = socket.gethostbyname(domain_name)
            self.logger.debug(f"fqdn: {domain_name} ip_address: {ip_address}")
            return ip_address
        except Exception as e:
            self.logger.error(f"Exception caught: {e}")
            return None
