"""
Module initialisation for the utility classses of dnsfix

Author: Jesmigel Cantos
"""

from .nslookup import NSUtils

__all__ = [
    "NSUtils",
]
