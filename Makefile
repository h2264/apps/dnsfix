# PYTHON TARGETS
.PHONY: isort-lint isort-format
APP_CMD=poetry run
PYTHON_LINE_LENGTH ?= 120
isort-format:
	$(APP_CMD) isort --profile black --line-length $(PYTHON_LINE_LENGTH) --skip-glob="*/__init__.py" src/

isort-lint:
	$(APP_CMD) isort --check-only --profile black --line-length $(PYTHON_LINE_LENGTH) --skip-glob="*/__init__.py" --py=310 src/

.PHONY: black-lint black-format
black-format:
	$(APP_CMD) black --exclude .+\.ipynb --line-length $(PYTHON_LINE_LENGTH) src/

black-lint:
	$(APP_CMD) black --exclude .+\.ipynb --check --line-length $(PYTHON_LINE_LENGTH)  src/

.PHONY: generic-lint generic-format
generic-format:
	$(APP_CMD) autopep8 --max-line-length $(PYTHON_LINE_LENGTH) --in-place -r src/

generic-lint:
	$(APP_CMD) pylint -vv --max-line-length $(PYTHON_LINE_LENGTH) src/

.PHONY: python-format python-lint python-lint-e2e

python-format: isort-format black-format generic-format

python-lint: isort-lint black-lint generic-lint

python-lint-e2e: python-format python-lint

# _PYTEST_FILTER="-k test_main"
python-test:
	poetry run pytest $(_PYTEST_FILTER) 

# DEV ENVIRONMENT
.PHONY: local-dev-env local-install-deps
BASE_OCI=python
BASE_TAG=3.12-slim-bullseye
BASE_IMG=python:3.12-slim-bullseye
PROJECT=dnsfix
local-install-deps:
	pip install poetry
	poetry install

local-dev-env:
	docker run -tid -v $(PWD):/app -v $(HOME)/.kube:/root/.kube -w /app --name $(PROJECT)_devenv --rm $(BASE_IMG) bash
	docker exec $(PROJECT)_devenv bash -c 'apt update -y && apt install -y make git'
	docker exec $(PROJECT)_devenv bash -c 'make local-install-deps'
	docker exec -ti $(PROJECT)_devenv bash

local-dev-clean:
	docker stop $(PROJECT)_devenv
	docker rm $(PROJECT)_devenv


BASE_OCI=python
BASE_TAG=3.12-slim-bullseye
BASE_IMG=python:3.12-slim-bullseye
OCI_ENGINE=docker
OCI_REGISTRY=ocr.jcan.dev/library
OCI_IMG=dnsfix
OCI_TAG=0.1.0
oci-build:
	$(OCI_ENGINE) build -t $(OCI_REGISTRY)/$(OCI_IMG):$(OCI_TAG) -f image/Dockerfile .
oci-publish:
	$(OCI_ENGINE) push $(OCI_REGISTRY)/$(OCI_IMG):$(OCI_TAG)
oci-clean:
	$(OCI_ENGINE) rmi $(OCI_REGISTRY)/$(OCI_IMG):$(OCI_TAG)

test:
	poetry run python -m dnsfix --config test.yaml