# dnsfix

DNSFIX is a python module intended to act as an interface that allows updating the public domains to match a dynamic ip address.

## Architecture

TBA (Draw IO).

## Development environment

### Requirements

- Host machine has container runtime engine (docker) installed.
- Host machine has make installed.

### Getting started

The following bash commands launches a containerised dev environment independent of the IDE.

```bash
# Clone and cd to repository
git clone --recursive https://gitlab.com/h2264/apps/dnsfix.git \
&& cd dnsfix
# Launch development environment
# make local-dev-env target performs the following
# - launches a container
# - installs dependencies
# - launches an interactive shell
make local-dev-env
```

## TESTING

```bash
# deploy dev env
make local-dev-env

# run end to end lint tests
make python-lint-e2e
```

## LICENSE

see [LICENSE](./LICENSE) file.

## TODO's

- [ ] generate architecture diagram
- [ ] develop architecture documentation
- [ ] develop event trigger for publishing to production
- [ ] develop event trigger for publishing to development
